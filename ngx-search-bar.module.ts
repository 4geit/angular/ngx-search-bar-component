import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxSearchBarComponent } from './ngx-search-bar.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxMaterialModule,
  ],
  declarations: [
    NgxSearchBarComponent
  ],
  exports: [
    NgxSearchBarComponent
  ]
})
export class NgxSearchBarModule { }
