import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

import { NgxMarketplaceProductsService } from '@4geit/ngx-marketplace-products-service';

@Component({
  selector: 'ngx-search-bar',
  template: require('pug-loader!./ngx-search-bar.component.pug')(),
  styleUrls: ['./ngx-search-bar.component.scss']
})
export class NgxSearchBarComponent implements OnInit {

  itemCtrl: FormControl;
  items = [
    'aaa',
    'bbb',
    'ccc'
  ];
  filteredItems: any;

  constructor(
    private productsService: NgxMarketplaceProductsService
  ) {
    this.itemCtrl = new FormControl();
    this.filteredItems = this.itemCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterItems(name))
    ;
  }

  filterItems(val: string) {
    return val ? this.productsService.items.filter(i => i.name.toLowerCase().indexOf(val.toLowerCase()) === 0) : this.productsService.items;
  }

  ngOnInit() {
  }

}
