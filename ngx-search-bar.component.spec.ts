import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxSearchBarComponent } from './ngx-search-bar.component';

describe('search-bar', () => {
  let component: search-bar;
  let fixture: ComponentFixture<search-bar>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ search-bar ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(search-bar);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
